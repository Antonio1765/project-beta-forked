# CarCar

Team:

* Walker Mills - Service Microservice
* Antonio Cervantes - Sales Microservice

## Design

## Service microservice

Made an Appointment and Technician model to represent the service needs.
Also created an AutomobileVO along with a poller to grab automobile data from the iventory microservice

Created view functions and encoders to handle the HTTP requests for creating, listing, and deleting instances of my models.

Finally, used JSX to create multiple form and list pages for our front end, which allows a customer to schedule an appointment, or by a car, and allows
the workers to add a technician, manufactuer, vehicle model, and an automobile, as well as see a history of sales and appointments.

## Sales microservice

Created a automobileVO to communicate with the inventory model, "Automobile".

We give the VO vin and sold attributes which we used on the frontend to dictate what a user would be able to see depending on the status of wether or not a car had been sold

Created a Salesperson model that included a salesperson's name and employee id.

Created a customer model that included a salesperson's name, address and phone number.

All of these models would then be used as foreignkeys for the Sale model.
