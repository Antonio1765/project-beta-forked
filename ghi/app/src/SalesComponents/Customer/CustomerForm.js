import React, { useState } from "react";

function CustomerForm () {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [address, setAddress] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");

    const handleChange = (setFunc) => {
        return (
            function (e) {
                setFunc(e.target.value);
            }
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const customerData = {
            "first_name": firstName,
            "last_name": lastName,
            "address": address,
            "phone_number": phoneNumber
        }

        const url = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(customerData),
            headers: {"Content-Type": "application/json"}
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFirstName("")
            setLastName("")
            setAddress("")
            setPhoneNumber("")
            document.getElementById("create-customer-form").reset()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-5">
                    <h1>Add Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setFirstName)} type="text" name="firstName" className="form-control" placeholder="first name" />
                            <label htmlFor="firstName">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setLastName)}  type="text" name="lastName" className="form-control" placeholder="last name" />
                            <label htmlFor="lastName">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setAddress)} type="text" name="address" className="form-control" placeholder="address" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setPhoneNumber)} type="text" name="phoneNumber" className="form-control" placeholder="phone number" />
                            <label htmlFor="phoneNumber">Phone #</label>
                        </div>
                        <button className="btn btn-success">Add Customer</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
