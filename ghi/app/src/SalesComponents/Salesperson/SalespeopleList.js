import { useState, useEffect } from "react";

function SalespeopleList () {
    const [salespeople, setSalespeople] = useState([])

    const fetchSalespeople = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/")

        if (response.ok) {
            const salespeopleData = await response.json()
            setSalespeople(salespeopleData.salespeople)
        } else {
            console.error(response)
        }
    }

    useEffect(() => {
        fetchSalespeople();
    }, []);

    return (
        <div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Employee id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salespeople.map((person, i) => {
                            const evens = i % 2 === 0;
                            const backgroundColor = evens ? "#EAEAEA" : "#fff";

                            return (
                                <tr key={ person.employee_id } style= {{ backgroundColor }}>
                                    <td>{ person.employee_id }</td>
                                    <td>{ person.first_name }</td>
                                    <td>{ person.last_name }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalespeopleList
