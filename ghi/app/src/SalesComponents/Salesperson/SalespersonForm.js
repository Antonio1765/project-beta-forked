import React, { useState } from "react";

function SalespersonForm () {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeId, setEmployee_id] = useState("");

    const handleChange = (setFunc) => {
        return (
            function (event) {
                setFunc(event.target.value);
            }
        )
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const salesperson_data = {
            "first_name": firstName,
            "last_name": lastName,
            "employee_id": employeeId
        }

        const url = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(salesperson_data),
            headers: {"Content-Type": "application/json"}
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()

            setFirstName("")
            setLastName("")
            setEmployee_id("")
            document.getElementById("create-salesperson-form").reset()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-5">
                    <h1>Add Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setFirstName)} type="text" name="firstName" className="form-control" placeholder="first name" />
                            <label htmlFor="firstName">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setLastName)}  type="text" name="lastName" className="form-control" placeholder="last name" />
                            <label htmlFor="lastName">Last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setEmployee_id)} type="text" name="employeeId" className="form-control" placeholder="employee id" />
                            <label htmlFor="employeeId">Employee id</label>
                        </div>
                        <button className="btn btn-success">Add Employee</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm;
