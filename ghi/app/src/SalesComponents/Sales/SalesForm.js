import React, { useState, useEffect } from "react";

function SalesForm () {
    const [automobile, setAutomobile] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const [customer, setCustomer] = useState([]);
    const [formData, setFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: ""
    })

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const fetchAutoData = async () => {
        const url = "http://localhost:8100/api/automobiles/"
        const res = await fetch(url)
        if (res.ok) {
            const data = await res.json()
            setAutomobile(data.autos.filter(prop => prop.sold === false))
        }
    }

    const fetchSalespersonData = async () => {
        const url = "http://localhost:8090/api/salespeople/"
        const res = await fetch(url)
        if (res.ok) {
            const data = await res.json()
            setSalesperson(data.salespeople)
        }
    }

    const fetchCustomerData = async () => {
        const url = "http://localhost:8090/api/customers/"
        const res = await fetch(url)
        if (res.ok) {
            const data = await res.json()
            setCustomer(data.customers)
        }
    }

    useEffect(() => {
        fetchAutoData();
        fetchSalespersonData();
        fetchCustomerData();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            automobile: `/api/automobiles/${formData.automobile}/`,
            salesperson: formData.salesperson,
            customer: formData.customer,
            price: formData.price
        }

        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        }



        const res = await fetch(url, fetchConfig)
        if (res.ok) {
            const newSale = await res.json()
            const fetchOptions = {
                method: "put",
                body: JSON.stringify({ "sold": true }),
                headers: {"Content-Type": "application/json"}
            }
            const invRes = await fetch(`http://localhost:8100/api/automobiles/${formData.automobile}/`, fetchOptions)
            setFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            })
            document.getElementById("create-sale-form").reset()
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-5">
                    <h1>Add Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select required={true} onChange={handleChange} name="automobile" className="form-select" >
                                <option value="">Vin #5</option>
                                {automobile.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select required={true} onChange={handleChange} name="salesperson" className="form-select" >
                                <option value="">Salesperson</option>
                                {salesperson.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select required={true} onChange={handleChange} name="customer" className="form-select" >
                                <option value="">Customer</option>
                                {customer.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange} type="text" name="price" className="form-control" placeholder="price" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-success">Add Sale</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm
