import { useState, useEffect } from "react";

function SalesList () {
    const [sales, setSales] = useState([])

    const fetchSales = async () => {
        const res = await fetch("http://localhost:8090/api/sales/")

        if (res.ok) {
            const data = await res.json()
            setSales(data.sales)
        } else {
            console.error(res)
        }
    }

    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((prop, i) => {
                        const evens = i % 2 === 0;
                        const backgroundColor = evens ? "#EAEAEA" : "#fff";
                        return (
                            <tr key={prop.id} style={{ backgroundColor }} >
                                <td>{ prop.salesperson.employee_id }</td>
                                <td>{ `${prop.salesperson.first_name} ${prop.salesperson.last_name}` }</td>
                                <td>{ `${prop.customer.first_name} ${prop.customer.last_name}` }</td>
                                <td>{ prop.automobile.vin }</td>
                                <td>{ prop.price }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalesList
