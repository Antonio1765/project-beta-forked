import { useState, useEffect } from "react";

function AutomobileList () {

    const [autos, setAutos] = useState([])
    const loadAutos = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok){
            const data = await response.json()
            setAutos(data.autos)
        } else {
            console.error(response)
        }
    }


    useEffect(() => {
        loadAutos();
    }, [])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>VIN</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.model.manufacturer.name + " " + auto.model.name}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.vin}</td>
                                <td>{auto.sold ? "Yes" : "No"}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default AutomobileList;
