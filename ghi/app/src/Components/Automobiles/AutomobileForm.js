import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function AutomobileForm () {

    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: ''
    })

    const loadModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        } else {console.error(response)}
    }

    const handleChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch('http://localhost:8100/api/automobiles/', {method: 'post', body: JSON.stringify(formData), headers: {'Content-Type': 'application/json'}})
        if (response.ok) {
            const newAuto = await response.json()
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: ''
            })
            document.getElementById('create-auto-form').reset()
        }
    }

    useEffect(() => {
        loadModels();
    },[])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/automobiles" end><button className="btn btn-success">Back</button></NavLink>
                    <h1>Create an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-auto-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} className="form-control" type="text" placeholder="Color" name="color" id="color"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} className="form-control" type="number" placeholder="year" name="year" id="year"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required={true} className="form-control" type="text" placeholder="VIN" name="vin" id="vin"/>
                            <label htmlFor="vin">VIN Number</label>
                        </div>
                        <div>
                            <select onChange={handleChange} required={true} className="form-select" name="model_id" id="model_id">
                                <option value="">Model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-success mt-3">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AutomobileForm;
