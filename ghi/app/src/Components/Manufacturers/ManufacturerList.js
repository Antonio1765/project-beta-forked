import { useEffect, useState } from "react";


function ManufacturerList () {

    const [manufacturers, setManu] = useState([])

    const loadManus = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok){
            const data = await response.json()
            setManu(data.manufacturers)
        } else {
            console.error(response)
        }
    }

    useEffect(() => {
        loadManus();
    }, [])

    const handleClick = async (event) => {
        const ManuID = event.target.value
        const response = await fetch(`http://localhost:8100/api/manufacturers/${ManuID}/`, {method: 'delete'})
        if (response.ok) {
            setManu(manufacturers.filter(item => item.id !== Number(ManuID)))
        }
    }

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                                <td><button value={manufacturer.id} onClick={handleClick} className="btn btn-outline-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;
