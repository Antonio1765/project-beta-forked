import { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function ManufacturerForm () {

    const [formData, setFormData] = useState({ name: '' })

    const handleChange = (event) => {
        setFormData({
            name: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const response = await fetch('http://localhost:8100/api/manufacturers/', {method: 'post', body: JSON.stringify(formData), headers: {'Content-Type': 'application/json'}})
        if (response.ok) {
            const newManu = await response.json()

            setFormData({name:''})
            document.getElementById('create-manufacturer-form').reset()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/manufacturers" end><button className="btn btn-success">Back</button></NavLink>
                    <h1>Add a manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} className="form-control" required={true} type="text" placeholder="Manufacturer Name" name="manufacturer name" id="manufacturer name"/>
                            <label htmlFor="manufacturer name">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
