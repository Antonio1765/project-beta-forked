import { useState, useEffect } from "react";


function TechnicianList () {

    const [techs, setTechs] = useState([])

    const loadTechs = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')

        if (response.ok){
            const data = await response.json()
            setTechs(data.technician)
        }
        else {
            console.error(response)
        }
    }

    const handleClick = async (event) => {
        const techID = event.target.value
        const url = `http://localhost:8080/api/technicians/${techID}/`
        const fetchConfig = {
            method: 'delete'
        }
        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setTechs(techs.filter(item => item.id !== Number(techID)))
        }

    }

    useEffect(() => {
        loadTechs();
    }, []);

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {techs.map(tech => {
                        return (
                            <tr key={tech.id}>
                                <td>{ tech.first_name }</td>
                                <td>{ tech.last_name }</td>
                                <td>{ tech.employee_id }</td>
                                <td><button value={tech.id} onClick={handleClick} className="btn btn-outline-danger" type="button">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
