import { useState } from "react";
import { NavLink } from "react-router-dom";

function TechnicianForm () {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const handleChange = (setFunction) => {
        return (
            function (event) {
                setFunction(event.target.value)
            }
        )
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            "first_name": firstName,
            "last_name": lastName,
            "employee_id": employeeID
        }

        const technicianUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }

        const response = await fetch(technicianUrl, fetchConfig)
        if (response.ok) {
            const newTechnician = await response.json()

            setFirstName('')
            setLastName('')
            setEmployeeID('')
            document.getElementById('create-technician-form').reset()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <NavLink to="/technicians" end><button className="btn btn-success">Back</button></NavLink>
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setFirstName)} placeholder="First Name" type="text" name="firstName" id="firstName" className="form-control"/>
                            <label htmlFor="firstName">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setLastName)} placeholder="Last Name" type="text" name="lastName" id="lastName" className="form-control"/>
                            <label htmlFor="firstName">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required={true} onChange={handleChange(setEmployeeID)} placeholder="Employee ID" type="text" name="employeeID" id="employeeID" className="form-control"/>
                            <label htmlFor="firstName">Employee ID</label>
                        </div>
                        <button type="submit" className="btn btn-success">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default TechnicianForm;
