import { useState, useEffect } from "react";

function VehicleModelList () {

    const [vehicleModels, setVehicleModels] = useState([])

    const loadModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/')
        if (response.ok){
            const data = await response.json()
            setVehicleModels(data.models)
        } else {
            console.error(response)
        }
    }

    const handleClick = async (event) => {
        const modelID = event.target.value
        const response = await fetch(`http://localhost:8100/api/models/${modelID}/`, {method: 'delete'})
        if (response.ok) {
            setVehicleModels(vehicleModels.filter(item => item.id !== Number(modelID)))
        }
    }

    useEffect(() => {
        loadModels();
    }, [])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Photo</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleModels.map(model =>{
                        return (
                            <tr key={model.id}>
                                <td>{model.manufacturer.name + " " + model.name}</td>
                                <td><img src={model.picture_url} width={225} height={150}/></td>
                                <td><button value={model.id} onClick={handleClick} className="btn btn-outline-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );

}
export default VehicleModelList;
