import { useState, useEffect } from "react"


function ServiceHistory () {

    const [appointments, setAppointments] = useState([])
    const [apps, setApps] = useState([])


    const loadApps = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
            setApps(data.appointments)
        } else {
            console.error(response)
        }
    }

    const handleClick = () => {
        const VIN = document.getElementById('search').value
        setApps(appointments.filter(item => item.vin === VIN))
    }

    useEffect(() => {
        loadApps();
    }, []);

    return (
        <div>
            <h1>Service History</h1>
            <div className="input-group mb-3">
                <input className="form-control" placeholder="Search by VIN" id='search' name='search'/>
                <button onClick={handleClick} className="btn btn-outline-secondary" id='button-addon2'>Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Reason</th>
                        <th>Date / Time</th>
                        <th>Technician</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {apps.map(appointment => {
                        const date = new Date(appointment.date_time)
                        const formatDate = date.toLocaleDateString('en-US', {
                            day: '2-digit',
                            month: '2-digit',
                            year: '2-digit'
                        })
                        return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.is_vip}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.reason}</td>
                            <td>{formatDate} </td>
                            <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                            <td>{appointment.status}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory
